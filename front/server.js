
const express = require('express');
const next = require('next');
const { createProxyMiddleware } = require("http-proxy-middleware");
const PORT = process.env.PORT || 3002;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

const apiTarget = `http://${dev ? process.env.API_LOCAL_HOST : process.env.API_HOST}:3000`;
const apiPaths = {
    '/articles': {
        target: apiTarget,
        pathRewrite: {
            '^/articles': '/articles'
        },
        changeOrigin: true
    },
    '/articles/:id': {
      target: apiTarget, 
      pathRewrite: {
          '^/articles/:id': '/articles/:id'
      },
      changeOrigin: true
  }
}

app.prepare().then(() => {
  const server = express()

  server.use('/articles', createProxyMiddleware(apiPaths['/articles']));
  server.use('/articles/:id', createProxyMiddleware(apiPaths['/articles/:id']));

  server.all('*', (req, res) => {
    return handle(req, res)
  })

  server.listen(PORT, (err) => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${PORT}`)
  })
})