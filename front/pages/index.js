import Head from 'next/head';
import React, { useEffect, useState } from 'react';
import {
  fetchArticles,
  deleteArticle
} from './api/articles';
import styles from '../styles/Home.module.css';
import Components from '../components';
const {
  Header,
  Articles
} = Components;

export default function Home() {
  const [articles, setArticles] = useState([]);
  const title = "Hackernews Feed";
  const description = "We love hacker news!";

  const handleArticles = async () => {
    const fetchedArticles = await fetchArticles();
    setArticles([...fetchedArticles]);
  }

  const handleDelete = async (id) => {
    try {
      const deletedArticle = await deleteArticle(id);
      const updatedArticles = articles.filter(article => article.rawTitle !== deletedArticle.rawTitle);
      setArticles([...updatedArticles]);
    } catch (err) {
      console.log(err);
    }
  }

  useEffect(() => {
    setTimeout(() => {
      handleArticles();
    }, 2000);
  }, [])

  return (
    <div className={styles.container}>
      <Head>
        <title>Hackernews Feed</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Header
        title={title}
        description={description}
      />
      <main className={styles.main}>
        <Articles
          articles={articles}
          handleDelete={handleDelete}
        />
      </main>
    </div>
  )
}
