export const fetchArticles = async () => {
  try {
    const response = await fetch('/articles');
    const articles = await response.json();
    return articles;
  } catch (err) {
    console.log(err);
    return []
  }
}

export const deleteArticle = async (id) => {
  try {
    const options = {
      method: 'POST',
    }
    const response = await fetch(`/articles/${id}`, options);
    const articles = await response.json();
    return articles;
  } catch (err) {
    console.log(err);
    throw err;
  }
}

