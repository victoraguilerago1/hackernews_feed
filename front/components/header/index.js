import React from 'react';
import PropTypes from 'prop-types';
import styles from '../../styles/Header.module.css';

function Header({
  title,
  description,
}) {
  return (
    <header className={styles.header}>
      <h1 className={styles.title}>
        {title}
      </h1>
      <h2 className={styles.description}>
        {description}
      </h2>
    </header>
  )
}

Header.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
}

export default Header;
