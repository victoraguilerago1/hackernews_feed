import React from 'react';
import PropTypes from 'prop-types';
import styles from '../../styles/Loader.module.css';

function Loader ({
  loading
}) {
  return (
    <div
      className={`${styles.circle} ${loading ? styles.active: ''}`}
    >
      <div></div><div></div><div></div><div></div>
    </div>
  )
}

Loader.propTypes = {
  loading: PropTypes.bool.isRequired,
}

export default Loader;
