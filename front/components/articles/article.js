import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Delete from '../icons/Delete';
import styles from '../../styles/Article.module.css';

function Article({
  step,
  id,
  title,
  author,
  story_title,
  story_url,
  url,
  comment_text,
  created_at,
  today,
  handleDelete,
}) {
  const [animated, setAnimated] = useState(false);
  const creationTime = new Date(created_at);
  const recently = creationTime.getDate() === today.getDate() && creationTime.getMonth() === today.getMonth();
  const yesterday = (creationTime.getDate() - today.getDate()) === -1;

  useEffect(() => {
    const timeout = setTimeout(() => {
      setAnimated(true);
    }, step * 200);
    return () => {
      clearTimeout(timeout);
    };
  }, []);

  const handleDeleteClick = (e) => {
    e.preventDefault();
    handleDelete(id);
  }

  const normalizedValue = (value) => {
    if (parseInt(value) > 9) return value;
    else return `0${value}`;
  }

  const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];

  return (
    <a
      id={id}
      href={story_url || url}
      target="_blank"
      rel="noreferrer"
      className={styles.container}
    >
      <article
        key={title}

        className={`${styles.article} ${animated ? styles.animated : ''}`}
      >
        <div className={styles.header}>
          <h3 className={styles.title}>
            {title || story_title}
          </h3>
          <p className={styles.author}>
            - {author} -
          </p>
          <p className={styles.date}>
            {
              recently && !yesterday && `${normalizedValue(creationTime.getHours())}:${normalizedValue(creationTime.getMinutes())} ${creationTime && creationTime.getHours() > 12 ? 'pm' : 'am'}`
            }
            {
              yesterday && 'yesterday'
            }
            {
              !recently && !yesterday && `${monthNames[creationTime.getMonth()].slice(0, 3)} ${normalizedValue(creationTime.getDate())}`
            }
            <Delete onClick={handleDeleteClick} />
          </p>
        </div>
        <div
          className={styles.content}
        >
          {comment_text}
        </div>
      </article>
    </a>
  )
}

Article.defaultProps = {
  url: '',
  story_url: '',
  story_title: '',
}

Article.propTypes = {
  title: PropTypes.string,
  id: PropTypes.string.isRequired,
  step: PropTypes.number.isRequired,
  handleDelete: PropTypes.func.isRequired,
  story_url: PropTypes.string,
  url: PropTypes.string,
  story_title: PropTypes.string,
  author: PropTypes.string.isRequired,
  comment_text: PropTypes.string.isRequired,
  created_at: PropTypes.string.isRequired,
  today: PropTypes.shape({
    getDate: PropTypes.func.isRequired,
    getMonth: PropTypes.func.isRequired,
  })
}

export default Article;
