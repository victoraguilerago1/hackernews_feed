import React from 'react';
import PropTypes from 'prop-types';
import styles from '../../styles/Articles.module.css';
import Article from './article';
import Loader from '../loader';

function Articles({
  articles,
  handleDelete,
}) {
  const loading = articles.length < 1;
  const today = new Date();
  return (
    <section 
      className={`${styles.container} ${loading && styles.active}`}
    >
      <Loader loading={loading} />
      {
        articles
        && !loading
        && articles.map((article, i) => (
        <Article
          step={i}
          id={article._id}
          today={today}
          key={article.created_at}
          title={article.title}
          story_title={article.story_title}
          story_url={article.story_url}
          url={article.url}
          created_at={article.created_at}
          author={article.author}
          handleDelete={handleDelete}
        />
      ))}
    </section>
  )
}

Articles.propTypes = {
  handleDelete: PropTypes.func.isRequired,
  articles: PropTypes.shape({
    title: PropTypes.string,
    story_url: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    story_title: PropTypes.string.isRequired,
    length: PropTypes.number.isRequired,
    map: PropTypes.func.isRequired,
  }).isRequired,
}

export default Articles;
