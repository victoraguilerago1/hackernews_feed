import Header from './header';
import Articles from './articles';

export default {
  Header,
  Articles,
}