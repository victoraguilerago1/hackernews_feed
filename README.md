# HackerNews Feed
 ***Live Demo:*** https://www.victoraguilerago.com/

Hackernews Feed is and application to watch the recently published articles related to NodeJs.
The app is divided in two artifacts:
- `API`
- `Front`

#### API

The API Server was develop using a NestJS instance that persist information in a MongoDB and exposes endpoints to fetch and delete articles.

#### FRONT

The Front App was develop with NextJS to render a list of items, each item redirects to the article url if one is supplied by the provider.

In order to run the project just clone the repository and run:
```sh
$ docker-compose up --build
```

After the image is built the `Front` app will be served at
```sh
https://localhost:8080
```

### Development

Open your favorite Terminal and run these commands.

### DB
**First Tab:**
```sh
$ docker-compose up mongo
```
### API
Edit the `NODE_ENV` variable in the ./api/.env file to match `NODE_ENV=development`
**Second Tab:**:
```sh
$ cd ./api
$ npm run start
```

### Front
Edit the `NODE_ENV` variable in the ./front/.env file to match `NODE_ENV=development` and run
**Third Tab:**
```sh
$ cd ./front
$ npm run start
```

The `Front` app will be served at
```sh
https://localhost:8080
```


### Todos

 - API
    - Separate the http request to a new service in order to test the request to providers independantly.
    - Write more test to reach a better coverage.
 - Front
    - Add a context manager to avoid components dependencies.
    - Implement styled components.
    - Write tests to reach a better coverage.
 

