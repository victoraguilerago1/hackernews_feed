const production = process.env.NODE_ENV === 'production';

export default () => ({
  port: parseInt(process.env.PORT, 10) || 3000,
  database: {
    MONGODB_URI: `mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${production ? process.env.DB_HOST : process.env.DB_LOCAL_HOST }:${process.env.DB_PORT}`,
  }
});