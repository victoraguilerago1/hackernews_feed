import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ArticlesDocument = Articles & Document;

@Schema()
export class Articles {
  @Prop({ required: true })
  created_at: string;

  @Prop()
  rawTitle: string;

  @Prop()
  title: string;

  @Prop()
  story_title: string;

  @Prop()
  author: string;

  @Prop()
  comment_text: string;

  @Prop({ required: true })
  story_url: string;

  @Prop({ required: true })
  url: string;

  @Prop({ required: true })
  deleted: boolean;
}

export const ArticlesSchema = SchemaFactory.createForClass(Articles);
