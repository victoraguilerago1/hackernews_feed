export interface Article {
  title: string,
  author: string,
  story_title: string,
  story_url: string,
  comment_text: string,
  created_at: string,
  deleted: boolean,
}

export interface DeleteArticleQuery {
  _id: string,
}

export interface FindAndUpdateOptions {
  upsert: boolean,
  new: boolean,
  setDefaultsOnInsert: boolean,
  useFindAndModify: boolean,
}

export interface FindAndUpdateQuery {
  $and: FindAndUpdateQueryConditions[],
}

export interface FindAndUpdateQueryConditions {
  rawTitle?: string,
  deleted?: boolean,
}

export interface FetchedArticles {
  data: any;
}
