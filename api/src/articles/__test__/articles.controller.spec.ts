import { HttpService } from '@nestjs/common';
import { Articles } from '../../schemas/articles.schema';
import { ArticlesController } from '../articles.controller';
import { ArticlesService } from '../articles.service';
import mockArticle from '../__mocks__/article.mock';

describe('ArticlesController', () => {
  let articlesController: ArticlesController;
  let articlesService: ArticlesService;
  let httpService: HttpService;

  beforeEach(() => {
    articlesService = new ArticlesService(httpService, Articles);
    articlesController = new ArticlesController(articlesService);
  });

  describe('findAll', () => {
    it('should return an array of articles', async () => {
      const result: Articles[] = [mockArticle];
      jest.spyOn(articlesService, 'findAll').mockImplementation(() => new Promise((resolve) => resolve(result)));

      expect(await articlesController.getStoredArticles()).toBe(result);
    });
  });

  describe('handleCron', () => {
    it('should return an array of articles', async () => {
      const result: Articles[] = [mockArticle];
      jest.spyOn(articlesService, 'fetchArticles').mockImplementation(() => new Promise((resolve) => resolve(result)));
      
      jest.spyOn(articlesService, 'prepareArticles').mockImplementation(() => [new Promise((resolve) => resolve(mockArticle))]);
      
      jest.spyOn(articlesService, 'storeArticles').mockImplementation(() => new Promise((resolve) => resolve(result)));


      expect(await articlesController.handleCron()).toBe(result);
    });
  });
});