import { Articles } from '../../schemas/articles.schema';

const mockArticle: Articles = {
  title: 'title',
  rawTitle: 'rawTitle',
  story_title: 'story_title',
  story_url: 'story_title',
  created_at: 'created_at',
  comment_text: 'comment_text',
  author: 'author',
  url: 'url',
  deleted: true,
}

export default mockArticle;
