export class UpdateArticleDto {
  id: string;
  deleted: boolean;
}