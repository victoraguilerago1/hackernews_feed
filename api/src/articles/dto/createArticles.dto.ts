export class CreateArticleDto {
  rawTitle: string;
  title: string;
  story_title: string;
  story_url: string;
  author: string;
  url: string;
  created_at: string;
  comment_text: string;
  deleted: boolean;
}