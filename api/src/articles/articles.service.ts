import { Injectable, Logger, HttpService } from '@nestjs/common';
import { Model } from 'mongoose';
import { map, catchError } from 'rxjs/operators';
import { InjectModel } from '@nestjs/mongoose';
import { Articles, ArticlesDocument } from '../schemas/articles.schema';
import { CreateArticleDto } from './dto/createArticles.dto';
import { UpdateArticleDto } from './dto/updateArticle.dto';
import {
  Article,
  DeleteArticleQuery,
  FindAndUpdateOptions,
  FindAndUpdateQuery,
  FetchedArticles,
} from '../interfaces/article.interface';
import { throwError } from 'rxjs';

@Injectable()
export class ArticlesService {
  constructor(
    private httpService: HttpService,
    @InjectModel(Articles.name) private articlesModel: Model<ArticlesDocument>,
  ) {};


  private readonly logger = new Logger(ArticlesService.name);


  async fetchArticles(): Promise<Article[]> {
    this.logger.log('Fetch New Articles')
    return this.httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
    .pipe(
      catchError((err: string) => {
        this.logger.error('Error fetching the new articles', err);
        return throwError(err)
      }),
      map((response: FetchedArticles) => {
        this.logger.log(response.data.hits.length + ' Articles Fetch')
        return response.data.hits;
      }),
      catchError((err: string) => {
        this.logger.error('Error mapping the new articles', err);
        return throwError(err)
      }),
    ).toPromise(); 
  }

  prepareArticles(articles: Article[]): Promise<Articles>[] {
    this.logger.log('Prepare Store Queries for new articles');
    const articlesToStore: Promise<Articles>[] = articles.map((article: Article) => 
      this.create({
        rawTitle: article.title || article.story_title,
        title: article.title,
        author: article.author,
        story_title: article.story_title,
        story_url: article.story_url,
        url: article.story_url,
        comment_text: article.comment_text,
        created_at: article.created_at,
        deleted: false,
      })
    );
    return articlesToStore;
  }

  async storeArticles(articlesToStore: Promise<Articles>[]): Promise<Articles[]> {
    this.logger.log('Execute Store Queries for new articles');
    const storedArticles: Articles[] = await Promise.all(articlesToStore);
    this.logger.log(storedArticles.length + ' new Articles Stored')
    return storedArticles;
  }

  async create(createArticleDto: CreateArticleDto): Promise<Articles> {
    const options: FindAndUpdateOptions = {
      upsert: true,
      new: true,
      setDefaultsOnInsert: true,
      useFindAndModify: false,
    };

    const query: FindAndUpdateQuery = {
      $and: [
        { rawTitle: createArticleDto.rawTitle },
        { deleted: false }
      ]
    };

    return this.articlesModel.findOneAndUpdate(
      query,
      createArticleDto,
      options,
    );
  }

  async findAll(): Promise<Articles[]> {
    return this.articlesModel.find({ deleted: false }).sort({created_at: -1}).exec();
  }

  async deleteArticle(updateArticleDto: UpdateArticleDto): Promise<Article> {
    const query: DeleteArticleQuery = {
      _id: updateArticleDto.id,
    };
    try {
      this.logger.debug('Delete stored article ' + updateArticleDto.id);
      const deletedArticle: Article = await this.articlesModel.findOneAndUpdate(
        query,
        updateArticleDto,
      );
      this.logger.debug('Deleted stored article ' + updateArticleDto.id);
      return deletedArticle;
    } catch (err) {
      this.logger.debug('Error deleting stored article' + updateArticleDto.id);
      this.logger.debug(err);
      throw err;
    }

  }
}
