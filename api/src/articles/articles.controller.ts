import { Controller, Get, Post, Param } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { ArticlesService } from './articles.service';
import { Articles } from 'src/schemas/articles.schema';
import { Article } from 'src/interfaces/article.interface';

@Controller('articles')
export class ArticlesController {
  constructor(private readonly articlesService: ArticlesService) {
    this.handleCron();
  }

  @Get()
  async getStoredArticles() {
    const articles: Article[] = await this.articlesService.findAll();
    return articles;
  }

  @Post(':id')
  async deleteStoreArticle(@Param() params) {
    try {
      const deletedArticle: Article = await this.articlesService.deleteArticle({ id: params.id, deleted: true });
      return deletedArticle;
    } catch (err) {
      return {
        error: err,
        message: 'Error deleting stored article',
      }
    }
  }

  @Cron('0 0 * ? * * *')
  async handleCron() {
    const articles: Article[] = await this.articlesService.fetchArticles();
    const preparedArticles: Promise<Articles>[] = this.articlesService.prepareArticles(articles);
    const storedArticles: Articles[] = await this.articlesService.storeArticles(preparedArticles);

    return storedArticles;
  }
}
