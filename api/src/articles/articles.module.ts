import { Module, HttpModule} from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticlesController } from './articles.controller';
import { ArticlesService } from './articles.service';
import { Articles, ArticlesSchema } from '../schemas/articles.schema';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([{ name: Articles.name, schema: ArticlesSchema }])
  ],
  controllers: [ArticlesController],
  providers: [ArticlesService],
})
export class ArticlesModule {}